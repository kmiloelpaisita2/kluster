# Kloustr

This project is an Angular application for managing features related to base cards.

## Versions
- Angular CLI: 11.2.19
- Angular: 11.2.14
- TypeScript: 4.1.5
- Node.js: 12.11.1
- Angular material: 11.2.12

## Setup

### Prerequisites
- Node.js and npm installed

### Installation
1. Clone the repository: `https://desarrolloeficaz.visualstudio.com/Kloustr%20LAB/_gitKloustr%20LAB`
2. Navigate to the project directory: `cd kloustr`
3. Install dependencies: `npm install`

## Development Server
To run the application locally, use the Angular CLI's development server.
ng serve


## Build

## Local Build
Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

### Production Build
Run `npm run build-kls` to build the project for production with optimizations enabled. The build artifacts will be stored in the `dist/` directory.
